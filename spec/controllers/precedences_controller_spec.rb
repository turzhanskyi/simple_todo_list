# frozen_string_literal: true
require 'rails_helper'

RSpec.describe(PrecedencesController, type: :controller) do
  authenticate_user
  it_renders_404 :edit, :update, :destroy
  let(:precedence) { create(:precedence) }

  describe 'authenticate user:' do
    it 'should have a current_user' do
      expect(subject.current_user).to_not(eq(nil))
    end
  end

  describe 'GET #index' do
    subject { get :index }
    context 'renders index template' do
      it 'is valid' do
        is_expected.to(have_http_status(:success))
        is_expected.to(render_template('index'))
      end
    end
  end

  describe 'GET #new' do
    subject { get :new }
    context 'renders new template' do
      it 'is valid' do
        is_expected.to(have_http_status(:success))
        is_expected.to(render_template('new'))
      end
    end
  end

  describe 'POST #create' do
    subject(:post_create) { post :create, params: params }
    subject { -> { post_create } }
    context 'with valid attributes' do
      let(:params) { { precedence: { title: 'Precedence2' } } }
      it 'create new precedence and Precedence.count+1' do
        is_expected.to(change(Precedence, :count).by(1))
      end
      it 'create new precedence and redirect to precedences_path' do
        post_create
        is_expected.to(redirect_to(precedences_path))
      end
    end

    context 'with invalid attributes' do
      let(:params) { { precedence: { name: '' } } }
      it 'does not create new Precedence' do
        is_expected.to(change(Precedence, :count).by(0))
      end
      it 'does not create new Precedence and render_template new' do
        post_create
        is_expected.to(render_template('new'))
      end
    end
  end

  describe 'PUT #update' do
    subject { put :update, params: params }
    context 'with valid attributes' do
      let(:params) do
        { id: precedence.to_param,
          precedence: { title: 'Precedence2' } }
      end
      it 'it update precedence and redirect to Precedences_path' do
        is_expected.to(redirect_to(precedences_path))
      end
    end

    context 'with invalid attributes' do
      let(:params) do
        { id: precedence.to_param,
          precedence: { title: '' } }
      end
      it 'it update precedence and render_template edit' do
        is_expected.to(render_template('edit'))
      end
    end
  end

  describe 'DELETE #destroy' do
    subject { delete :destroy, params: { id: precedence.to_param } }
    context 'with valid attributes' do
      it 'destroy precedence and Precedence.count-1' do
        params = { id: precedence.to_param }
        expect do
          delete :destroy, params: params
        end.to(change(Precedence, :count).by(-1))
      end

      context 'with valid attributes' do
        it 'destroy precedence and redirect to Precedence_path' do
          is_expected.to(redirect_to(precedences_path))
        end
      end
    end
  end
end
