# frozen_string_literal: true
require 'rails_helper'

RSpec.describe(ProjectsController, type: :controller) do
  authenticate_user
  it_renders_404 :edit, :update, :destroy
  let(:project) { create(:project) }

  describe 'authenticate user:' do
    it 'should have a current_user' do
      expect(subject.current_user).to_not(eq(nil))
    end
  end

  describe 'GET #index' do
    subject { get :index }
    context 'renders index template' do
      it 'is valid' do
        is_expected.to(have_http_status(:success))
        is_expected.to(render_template('index'))
      end
    end
  end

  describe 'GET #new' do
    subject { get :new }
    context 'renders new template' do
      it 'is valid' do
        is_expected.to(have_http_status(:success))
        is_expected.to(render_template('new'))
      end
    end
  end

  describe 'POST #create' do
    let(:user) { create(:user) }
    subject { -> { post :create, params: params } }
    context 'with valid attributes' do
      let(:params) { { project: { title: 'Project2', user_id: user.to_param } } }

      it 'create new project and Project.count+1' do
        is_expected.to(change(Project, :count).by(1))
      end

      it 'create new project and redirect to projects_path' do
        post :create, params: params
        is_expected.to(redirect_to(projects_path))
      end
    end

    context 'with invalid attributes' do
      let(:params) { { project: { name: '' } } }

      it 'does not create new project' do
        is_expected.to(change(Project, :count).by(0))
      end

      it 'does not create new project and render_template new' do
        post :create, params: params
        is_expected.to(render_template('new'))
      end
    end
  end

  describe 'PUT #update' do
    subject { put :update, params: params }
    context 'with valid attributes' do
      let(:params) do
        { id: project.to_param,
          project: { title: 'Project2' } }
      end
      it 'it update project and redirect to projects_path' do
        is_expected.to(redirect_to(projects_path))
      end
    end

    context 'with invalid attributes' do
      let(:params) do
        { id: project.to_param,
          project: { title: '' } }
      end
      it 'it update project and render_template edit' do
        is_expected.to(render_template('edit'))
      end
    end
  end

  describe 'DELETE #destroy' do
    subject { delete :destroy, params: { id: project.to_param } }
    context 'with valid attributes' do
      it 'destroy project and Project.count-1' do
        params = { id: project.to_param }
        expect do
          delete :destroy, params: params
        end.to(change(Project, :count).by(-1))
      end

      context 'with valid attributes' do
        it 'destroy project and redirect to project_path' do
          is_expected.to(redirect_to(projects_path))
        end
      end
    end
  end
end
