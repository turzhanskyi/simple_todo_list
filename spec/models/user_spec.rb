# frozen_string_literal: true
# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  confirmation_sent_at   :datetime
#  confirmation_token     :string
#  confirmed_at           :datetime
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  login                  :string           default(""), not null
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  unconfirmed_email      :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_users_on_confirmation_token    (confirmation_token) UNIQUE
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_login                 (login) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#
require 'rails_helper'

RSpec.describe(User, type: :model) do
  subject { create(:user) }

  describe 'it has fields' do
    it { expect(subject).to(respond_to(:email)) }
    it { expect(subject).to(respond_to(:login)) }
    it { expect(subject).to(respond_to(:encrypted_password)) }
    it { expect(subject).to(respond_to(:confirmation_sent_at)) }
    it { expect(subject).to(respond_to(:confirmation_token)) }
    it { expect(subject).to(respond_to(:confirmed_at)) }
    it { expect(subject).to(respond_to(:remember_created_at)) }
    it { expect(subject).to(respond_to(:reset_password_token)) }
    it { expect(subject).to(respond_to(:reset_password_sent_at)) }
    it { expect(subject).to(respond_to(:unconfirmed_email)) }
  end

  describe 'it has associations' do
    it { expect(subject).to(have_many(:projects)) }
  end

  describe 'it expects valid object' do
    it { expect(subject).to(be_valid) }
  end

  describe 'when email is not present' do
    it 'it does not valid' do
      subject.email = ''
      expect(subject).to_not(be_valid)
    end
  end
end
