# frozen_string_literal: true
# == Schema Information
#
# Table name: tasks
#
#  id            :bigint           not null, primary key
#  deadline      :date
#  description   :text
#  title         :text             not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  precedence_id :bigint           default(2)
#  project_id    :bigint
#  status_id     :bigint           default(1)
#
# Indexes
#
#  index_tasks_on_precedence_id  (precedence_id)
#  index_tasks_on_project_id     (project_id)
#  index_tasks_on_status_id      (status_id)
#
require 'rails_helper'

RSpec.describe(Task, type: :model) do
  subject { create(:task) }

  describe 'it has fields' do
    it { expect(subject).to(respond_to(:title)) }
    it { expect(subject).to(respond_to(:description)) }
    it { expect(subject).to(respond_to(:deadline)) }
    it { expect(subject).to(respond_to(:precedence_id)) }
    it { expect(subject).to(respond_to(:project_id)) }
    it { expect(subject).to(respond_to(:status_id)) }
  end

  describe 'it has associations' do
    it { expect(subject).to(belong_to(:project)) }
    it { expect(subject).to(belong_to(:precedence)) }
    it { expect(subject).to(belong_to(:status)) }
    it { expect(subject).to(have_many(:tinymce_images)) }
  end

  describe 'it expects valid object' do
    it { expect(subject).to(be_valid) }
  end

  describe 'when title is not present' do
    it 'it does not valid' do
      subject.title = ''
      expect(subject).to_not(be_valid)
    end
  end
end
