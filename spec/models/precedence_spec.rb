# frozen_string_literal: true
# == Schema Information
#
# Table name: precedences
#
#  id         :bigint           not null, primary key
#  title      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require 'rails_helper'

RSpec.describe(Precedence, type: :model) do
  subject { create(:precedence) }

  describe 'it has fields' do
    it { expect(subject).to(respond_to(:title)) }
  end

  describe 'it has associations' do
    it { expect(subject).to(have_many(:tasks)) }
  end

  describe 'it expects valid object' do
    it { expect(subject).to(be_valid) }
  end

  describe 'when title is not present' do
    it 'it does not valid' do
      subject.title = ''
      expect(subject).to_not(be_valid)
    end
  end
end
