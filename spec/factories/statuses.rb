# frozen_string_literal: true
# == Schema Information
#
# Table name: statuses
#
#  id         :bigint           not null, primary key
#  title      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
FactoryBot.define do
  factory :status do
    title { 'Новий' }
  end
end
