# frozen_string_literal: true
# == Schema Information
#
# Table name: tasks
#
#  id            :bigint           not null, primary key
#  deadline      :date
#  description   :text
#  title         :text             not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  precedence_id :bigint           default(2)
#  project_id    :bigint
#  status_id     :bigint           default(1)
#
# Indexes
#
#  index_tasks_on_precedence_id  (precedence_id)
#  index_tasks_on_project_id     (project_id)
#  index_tasks_on_status_id      (status_id)
#
FactoryBot.define do
  factory :task do
    title { 'Завдання №1' }
    deadline { '27.04.2020' }
    project
    status
    precedence
  end
end
