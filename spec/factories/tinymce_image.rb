# frozen_string_literal: true
# == Schema Information
#
# Table name: tinymce_images
#
#  id         :bigint           not null, primary key
#  file       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  task_id    :bigint
#
# Indexes
#
#  index_tinymce_images_on_task_id  (task_id)
#
FactoryBot.define do
  factory :tinymce_image do
    file { 'Картинка' }
  end
end
