# frozen_string_literal: true
Rails.application.routes.draw do
  devise_for :users

  root to: 'projects#index'

  resources :projects do
    resources :tasks do
      patch :done, on: :member
    end
  end

  namespace :api do
    resource :tinymce_images, only: :create
  end

  resources :precedences

  match '*a', to: 'errors#not_found', via: :all

  mount LetterOpenerWeb::Engine, at: "/letter_opener"
end
