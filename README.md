# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
##### ruby 2.6.5p114 (2019-10-01 revision 67812) [x86_64-linux]
* Rails version
##### Rails 5.2.4.2
* Database
##### rails db:setup
* Run project on a local server
##### rails server
### Demo App: <https://nameless-springs-87122.herokuapp.com/users/sign_in>

### Video instruction: <https://drive.google.com/open?id=1cfj6RT1ybbXH_g3EuqBAn-zfv-lP2g3z>
###
* Test authentication user: 
#### (email: "turzhansky81@gmail.com",  password: "123456")
#### (email: "test@gmail.com",          password: "test1234") 
 


# Task manager

I'm a person who passionate about my own productivity. 
I want to manage my tasks and projects more effectively. 
I need a simple tool that supports me in controlling my task-flow

## Functional requirements

- I want to be able to create ​/ ​ update​ / ​ delete projects
- I want to be able to add tasks to my project
- I want to be able to update​ / ​ delete tasks
- I want to be able to prioritize tasks into a project
- I want to be able to choose deadline for my task
- I want to be able to mark a task as 'done'

## Technical requirements

#### 01. It should be a WEB application
#### 02. For the client side must be used: HTML, CSS (any libs as Twitter Boorstrap, Blueprint ...), JavaScript (any libs as jQuery, Prototype ...)
#### 03. For a server side any language as Ruby
#### 04. It should have a client side and server side validation

## Additional requirements
- It should work like one page WEB application and should use AJAX
technology, load and submit data without reloading a page.
- It should have user authentication solution and a user should only
have access to their own projects and tasks.
- It should have automated tests for the all functionality
### SQL task
### Given tables:
#### 01​. users (id, login, email, password and other Devise filed)
#### 02​. projects (id, title, user_id)
#### 03. precedences (id, title) 
#### 04. statuses (id, title)
#### 05. tinymce_images (id, file, task_type, task_id)
#### 06. ​tasks (id, title, description, deadline, status_id, precedence_id,  project_id)