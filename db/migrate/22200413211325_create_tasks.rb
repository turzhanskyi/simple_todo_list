# frozen_string_literal: true
class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.text(:title, null: false)
      t.text(:description, null: true)
      t.date(:deadline, null: true)
      t.belongs_to(:project, index: true)
      t.belongs_to(:status, index: true, default: 1)
      t.belongs_to(:precedence, index: true, default: 2)

      t.timestamps
    end
  end
end
