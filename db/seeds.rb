# frozen_string_literal: true

require './db/seeds/users'
require './db/seeds/projects'
require './db/seeds/precedences'
require './db/seeds/statuses'
require './db/seeds/tasks'
