# frozen_string_literal: true
if Status.count.zero?
  puts 'Seeding Statuses'
  Status.create!(title: 'нове')
  Status.create!(title: 'виконується')
  Status.create!(title: 'завершене')
  Status.create!(title: 'виконане')
end
