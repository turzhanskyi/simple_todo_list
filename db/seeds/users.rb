# frozen_string_literal: true
if User.count.zero?
  puts 'Seeding Users'
  User.create!(email: "turzhansky81@gmail.com", login: "TurVit", password: "123456", password_confirmation: "123456", confirmed_at: "2020-04-20 14:11:15.520128")
  User.create!(email: "test@gmail.com", login: "TestUser", password: "test1234", password_confirmation: "test1234", confirmed_at: "2020-04-20 14:11:16.520130")
end
