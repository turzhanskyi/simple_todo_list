# frozen_string_literal: true
if Task.count.zero?
  puts 'Seeding Tasks'
  Task.create!(title: 'Завдання №1', description: 'Детальний опис завдання', deadline: Date.today, project_id: 1, status_id: 1)
  Task.create!(title: 'Завдання №2', deadline: Date.today, project_id: 1, status_id: 4)
  Task.create!(title: 'Завдання №3', deadline: Date.today, project_id: 2, status_id: 2)
  Task.create!(title: 'Завдання №4', description: 'Детальний опис завдання', deadline: Date.today, project_id: 2, status_id: 4)

  Task.create!(title: 'Завдання №1', description: 'Детальний опис завдання', deadline: Date.today, project_id: 3, status_id: 4)
  Task.create!(title: 'Завдання №1.1', deadline: Date.today, project_id: 3, status_id: 3)
  Task.create!(title: 'Завдання №1.2', deadline: Date.today, project_id: 3, status_id: 3)
  Task.create!(title: 'Завдання №1.3', deadline: Date.today, project_id: 3, status_id: 3)
  Task.create!(title: 'Завдання №2', deadline: Date.today, project_id: 3, status_id: 4)

  Task.create!(title: 'Завдання №1', description: 'Детальний опис завдання', deadline: Date.today, project_id: 4, status_id: 4)
  Task.create!(title: 'Завдання №2', deadline: Date.today, project_id: 4, status_id: 4)

  Task.create!(title: 'Завдання №1', description: 'Детальний опис завдання', deadline: Date.today, project_id: 5, status_id: 4)
  Task.create!(title: 'Завдання №1.1', deadline: Date.today, project_id: 5, status_id: 3)
  Task.create!(title: 'Завдання №1.2', deadline: Date.today, project_id: 5, status_id: 3)
  Task.create!(title: 'Завдання №1.3', deadline: Date.today, project_id: 5, status_id: 3)
  Task.create!(title: 'Завдання №2', deadline: Date.today, project_id: 5, status_id: 4)

  Task.create!(title: 'Завдання №1', description: 'Детальний опис завдання', deadline: Date.today, project_id: 6, status_id: 4)
  Task.create!(title: 'Завдання №2', deadline: Date.today, project_id: 6, status_id: 4)
end
