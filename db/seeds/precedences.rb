# frozen_string_literal: true
if Precedence.count.zero?
  puts 'Seeding Precedences'
  Precedence.create!(title: 'низький')
  Precedence.create!(title: 'середній')
  Precedence.create!(title: 'високий')
end
