# frozen_string_literal: true
if Project.count.zero?
  puts 'Seeding Projects'
  Project.create!(title: 'Проєкт №3', user_id: 1)
  Project.create!(title: 'Проєкт №2', user_id: 1)
  Project.create!(title: 'Проєкт №1', user_id: 1)
  Project.create!(title: 'For Home', user_id: 1)
  Project.create!(title: 'Complete the test task', user_id: 1)

  Project.create!(title: 'Проєкт №1', user_id: 2)
end
