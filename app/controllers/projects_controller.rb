# frozen_string_literal: true
class ProjectsController < ApplicationController
  before_action :project, only: %i[show edit update destroy]

  def index
    @search = Project.all.where(user_id: authenticate_user!).ransack(params[:search])
    @search.sorts = ['name asc', 'created_at desc'] if @search.sorts.empty?
    @projects = @search.result.includes(:user, @tasks).paginate(page: params[:page], per_page: 3)
    @task = Task.new
  end

  def new
    @project = Project.new
  end

  def create
    @project = Project.new(project_params)

    respond_to do |format|
      if @project.save
        flash[:success] = t('activerecord.models.project.create.flash.success')
        format.html { redirect_to projects_path }
        format.json { render :index, status: :created, location: @project }
      else
        flash[:danger] = t('activerecord.models.project.create.flash.danger')
        format.html { render :new }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @project.update(project_params)
        flash[:success] = t('activerecord.models.project.update.flash.success')
        format.html { redirect_to projects_path }
        format.json { render :index, status: :ok, location: @project }
      else
        flash[:danger] = t('activerecord.models.project.update.flash.danger')
        format.html { render :edit }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @project.destroy
    respond_to do |format|
      flash[:success] = t('activerecord.models.project.destroy.flash.success')
      format.html { redirect_to projects_path }
      format.json { head :no_content }
    end
  end

  private

  def project
    @project ||= begin
                   Project.find(params[:id])
                 rescue StandardError
                   nil
                 end
    render_404 unless @project
  end

  def project_params
    params.require(:project).permit(:title, :user_id)
  end
end
