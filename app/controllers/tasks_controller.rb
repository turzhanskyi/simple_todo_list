# frozen_string_literal: true
class TasksController < ApplicationController
  before_action :task, only: %i[show edit update destroy done]
  before_action :project, only: %i[edit done]

  def create
    task
    task.assign_attributes(task_params)

    respond_to do |format|
      if task.save
        flash[:success] = t('activerecord.models.task.create.flash.success')
        format.html { redirect_to projects_path }
        format.json { render :index, status: :created, location: @task }
      else
        flash[:danger] = t('activerecord.models.task.create.flash.danger')
        format.html { redirect_to projects_path }
        format.json { render json: task.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    task
  end

  def update
    respond_to do |format|
      if task.update(task_params)
        flash[:success] = t('activerecord.models.task.update.flash.success')
        format.html { redirect_to projects_path }
        format.json { render :index, status: :ok, location: task }
      else
        flash[:danger] = t('activerecord.models.task.update.flash.danger')
        format.html { render :edit }
        format.json { render json: task.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    respond_to do |format|
      if task.destroy
        flash[:success] = t('activerecord.models.task.destroy.flash.success')
        format.html { redirect_to projects_path }
        format.json { head :no_content }
      else
        flash[:danger] = task.errors.full_messages
        format.html { redirect_to projects_path }
      end
    end
  end

  def done
    if task.update(status_id: params[:task][:status])
      redirect_to(projects_path, notice: t('activerecord.models.task.done.flash.success_1')) if params[:task][:status] == '4'
      redirect_to(projects_path, notice: t('activerecord.models.task.done.flash.success_2')) if params[:task][:status] == '2'
    else
      render(projects_path, danger: [t('activerecord.models.task.done.flash.danger'), task.errors.full_messages.join(',')].join)
    end
  end

  private

  def task
    @task ||= params[:id].present? ? tasks.find(params[:id]) : tasks.new
  end

  def tasks
    @tasks ||= project.tasks
  end

  def project
    @project ||= begin
                   Project.find(params[:project_id])
                 rescue StandardError
                   nil
                 end
  end

  def task_params
    params.require(:task).permit(:project_id, :title, :description, :deadline, :precedence_id, :status_id, :tinymce_image)
  end
end
