# frozen_string_literal: true
class PrecedencesController < ApplicationController
  before_action :precedence, only: %i[show edit update destroy]

  def index
    @precedences = Precedence.all
  end

  def new
    @precedence = Precedence.new
  end

  def create
    @precedence = Precedence.new(precedence_params)

    respond_to do |format|
      if @precedence.save
        flash[:success] = t('activerecord.models.precedence.create.flash.success')
        format.html { redirect_to precedences_path }
        format.json { render :index, status: :created, location: @precedence }
      else
        flash[:danger] = t('activerecord.models.precedence.create.flash.danger')
        format.html { render :new }
        format.json { render json: @precedence.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @precedence.update(precedence_params)
        flash[:success] = t('activerecord.models.precedence.update.flash.success')
        format.html { redirect_to precedences_path }
        format.json { render :index, status: :ok, location: @precedence }
      else
        flash[:danger] = t('activerecord.models.precedence.update.flash.danger')
        format.html { render :edit }
        format.json { render json: @precedence.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @precedence.destroy
    respond_to do |format|
      flash[:success] = t('activerecord.models.precedence.destroy.flash.success')
      format.html { redirect_to precedences_path }
      format.json { head :no_content }
    end
  end

  private

  def precedence
    @precedence ||= begin
                      Precedence.find(params[:id])
                    rescue StandardError
                      nil
                    end
    render_404 unless @precedence
  end

  def precedence_params
    params.require(:precedence).permit(:title)
  end
end
