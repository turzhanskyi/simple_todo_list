# frozen_string_literal: true
class ErrorsController < ApplicationController
  def not_found
    respond_to do |format|
      format.json { render json: { error: 404 }, status: :not_found }
      format.html { render(status: :not_found) }
    end
  end
end
