# frozen_string_literal: true
module Api
  class TinymceImagesController < ApplicationController
    def create
      image = TinymceImage.new(tinymce_image_params)

      if image.save
        render(json: { image: { url: image.file.url } }, content_type: 'text/html')
      else
        render(json: { error: { message: image.errors.full_messages.join(', ') } })
      end
    end

    private

    def tinymce_image_params
      params.permit(:file, :task_id)
    end
  end
end
