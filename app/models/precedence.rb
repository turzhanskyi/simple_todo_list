# frozen_string_literal: true
# == Schema Information
#
# Table name: precedences
#
#  id         :bigint           not null, primary key
#  title      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Precedence < ApplicationRecord
  has_many :tasks, dependent: :destroy

  validates :title, presence: true
end
