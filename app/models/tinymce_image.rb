# frozen_string_literal: true
# == Schema Information
#
# Table name: tinymce_images
#
#  id         :bigint           not null, primary key
#  file       :string
#  task_type  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  task_id    :bigint
#
# Indexes
#
#  index_tinymce_images_on_task_type_and_task_id  (task_type,task_id)
#
class TinymceImage < ApplicationRecord
  mount_uploader :file, TinymceImageUploader
end
